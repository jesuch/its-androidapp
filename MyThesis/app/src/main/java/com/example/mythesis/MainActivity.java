package com.example.mythesis;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TextView Start
        TextView tv = (TextView)findViewById(R.id.tvAbout);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent fbIntent = aboutus(MainActivity.this);
                startActivity(fbIntent);

            }
        });
        //TextView End


        //Button start
        Button button = (Button) findViewById(R.id.btnLogin);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(i);
            }
        });
        //Button end


    }
    public static  Intent aboutus(Context context){
        try{
            context.getPackageManager()
                    .getPackageInfo("com.example.mythesis",0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/589708217841908"));
        } catch (PackageManager.NameNotFoundException e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/sundayelephant/"));
        }
    }
}

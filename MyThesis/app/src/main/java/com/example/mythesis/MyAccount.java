package com.example.mythesis;

import android.renderscript.Sampler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MyAccount extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private DatabaseReference mDatabase2;
    private DatabaseReference mDatabase3;
    private TextView mNameView;
    private TextView mJobPosView;
    private EditText mNameEditTextView;
    private Button mButton;

    Employee employee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Name");
        mDatabase2 = FirebaseDatabase.getInstance().getReference().child("Job Position");
        mDatabase3 = FirebaseDatabase.getInstance().getReference().child("Employee");

        mNameView = findViewById(R.id.name_view);
        mJobPosView = findViewById(R.id.jobpos_view);
        mNameEditTextView = findViewById(R.id.name_et_view);
        mButton = findViewById(R.id.add_emp_view);

        employee = new Employee();



        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String name = dataSnapshot.getValue().toString();
                mNameView.setText(name);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDatabase2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String jobPosition = dataSnapshot.getValue().toString();
                mJobPosView.setText(jobPosition);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                employee.setName(mNameEditTextView.getText().toString().trim());
                employee.setJob_Position(mJobPosView.getText().toString().trim());

                // use .child kung isang row lang sa db
               // mDatabase3.child("Employee Details").setValue(employee);

                // use push without child kung many data ang ipupush
                mDatabase3.push().setValue(employee);

                mJobPosView.setText(employee.getName());


                Toast.makeText(MyAccount.this,"Employee Added", Toast.LENGTH_LONG).show();
            }
        });
    }
}

package com.example.mythesis;

public class Employee {
    private String Name;
    private String Job_Position;

    public Employee() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getJob_Position() {
        return Job_Position;
    }

    public void setJob_Position(String job_Position) {
        Job_Position = job_Position;
    }
}
